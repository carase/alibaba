package com.shpowernode.springcloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.ConfigurableApplicationContext;

@EnableDiscoveryClient // 开启服务发现与注册
@SpringBootApplication
public class ConfigApplication  {

    public static void main(String[] args) {
        ConfigurableApplicationContext applicationContext = SpringApplication.run(ConfigApplication.class, args);
        while(true){
            String username = applicationContext.getEnvironment().getProperty("username");
            String userage = applicationContext.getEnvironment().getProperty("userage");
            System.out.println("username=" + username + ",userage=" + userage);

            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

    }

}
