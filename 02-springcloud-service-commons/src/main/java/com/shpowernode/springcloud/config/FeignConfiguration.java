package com.shpowernode.springcloud.config;

import feign.auth.BasicAuthRequestInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class FeignConfiguration {

    // 一种契约，采用feign的契约方式，如果不配置该Bean，会转成SpringMVC的方式
    // @RequestLine("GET /service/student")
    // @Bean
    // public Contract feignContract(){
    //     return new Contract.Default();
    // }

    @Bean
    public BasicAuthRequestInterceptor basicAuthRequestInterceptor(){
        return new BasicAuthRequestInterceptor("orange","123456");
    }


}
