package com.shpowernode.springcloud.service;

import com.shpowernode.springcloud.config.FeignConfiguration;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(value="SPRINGCLOUD-SERVICE-GOODS",
             /*fallback = StudentRemoteClientFallBack.class*/
             fallbackFactory = StudentRemoteClientFallBackFactory.class,
             configuration = FeignConfiguration.class /*自定义feign配置客户端*/)
public interface StudentRemoteClient {

    /**
     *  声明一个feign接口，实现者是服务提供者的controller实现
     * @param id
     * @return
     */
    @GetMapping("/query/{id}")
    public String query(@PathVariable Integer id);
}
