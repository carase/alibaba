package com.shpowernode.springcloud.service;

import com.shpowernode.springcloud.pojo.Student;
import org.springframework.stereotype.Component;

@Component
 public class StudentRemoteClientFallBack implements StudentRemoteClient{

    /**
     *  服务降级方法
     * @param id
     * @return
     */
    public String query(Integer id) {
        return new Student() + "Feign  服务降级方法！";
    }
}
