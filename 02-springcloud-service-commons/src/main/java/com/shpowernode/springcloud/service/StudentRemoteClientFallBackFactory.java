package com.shpowernode.springcloud.service;

import com.shpowernode.springcloud.pojo.Student;
import feign.hystrix.FallbackFactory;
import org.springframework.stereotype.Component;

@Component
public class StudentRemoteClientFallBackFactory implements FallbackFactory<StudentRemoteClient> {
    @Override
    public StudentRemoteClient create(Throwable throwable) {
        return new StudentRemoteClient() {
            @Override
            public String query(Integer id) {
                String message = throwable.getMessage();
                System.out.println("feign远程调用异常" + message);
                return new Student().toString() + "服务异常！";
            }
        };
    }
}
