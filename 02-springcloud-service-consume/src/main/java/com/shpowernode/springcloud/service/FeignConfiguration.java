package com.shpowernode.springcloud.service;


import org.springframework.context.annotation.Bean;

public class FeignConfiguration {

    @Bean
    public EchoServiceFallBack echoServiceFallBack(){
        return new EchoServiceFallBack();
    }
}
