package com.shpowernode.springcloud.service;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

@FeignClient(value="spring-service-provider",
             fallback = EchoServiceFallBack.class,
             configuration = FeignConfiguration.class /*自定义feign配置客户端*/)

public interface EchoService {

    /**
     *  声明一个feign接口，实现者是服务提供者的controller实现
     */
    @GetMapping("/")
    public String get();


    @GetMapping("/notFound")
    public String notFound();

    default String notFound(Integer id){
        return "echoService默认实现！";
    }
}
