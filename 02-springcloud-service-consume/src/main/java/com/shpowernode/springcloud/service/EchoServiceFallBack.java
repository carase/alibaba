package com.shpowernode.springcloud.service;

import org.springframework.stereotype.Component;

@Component
 public class EchoServiceFallBack implements EchoService {

    /**
     *  服务降级方法
     */
    @Override
    public String get() {
        return "Feign服务降级！";
    }

    @Override
    public String notFound() {
        return "未实现Feign服务降级！";
    }
}
