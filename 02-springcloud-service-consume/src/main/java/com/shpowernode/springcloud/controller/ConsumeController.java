package com.shpowernode.springcloud.controller;

import com.alibaba.nacos.client.utils.JSONUtils;
import com.shpowernode.springcloud.service.EchoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.cloud.client.loadbalancer.LoadBalancerClient;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpServletRequest;

@RefreshScope
@RestController
public class ConsumeController {

    @Autowired
    private LoadBalancerClient loadBalancerClient;

    // 模板类可以进行http接口调用
    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private DiscoveryClient discoveryClient;

    @Autowired
    private EchoService echoService;

    @GetMapping("/echo/consumer")
    public String query(){
        ServiceInstance serviceInstance = loadBalancerClient.choose("spring-service-provider");

        String url = String.format("http://%s:%s/",serviceInstance.getHost(),serviceInstance.getPort());
        System.out.println("url=" + url);
        return restTemplate.getForObject(url,String.class);
    }

    @GetMapping("/echo-rest/consumer")
    public String queryRest(HttpServletRequest request){
        System.out.println("Request-Id = " + request.getHeader("Request-Id"));
        System.out.println("Request-color = " + request.getHeader("Request-color"));
        System.out.println("color = " + request.getParameter("color"));
        return restTemplate.getForObject("http://spring-service-provider",String.class);
    }

    @GetMapping("/echo-rest/consumer2") // 直接方式使用负载均衡无法调用
    public String queryRest2(){
        return restTemplate.getForObject("http://192.168.18.1:10010",String.class);
    }

    @GetMapping("/echo-rest-entity/consumer")
    public String queryRestEntity(){
        ResponseEntity<String> entity = restTemplate.getForEntity("http://spring-service-provider", String.class);
        return entity.getBody();
    }

    @GetMapping("/feign/consumer")
    public String feign(){
        return echoService.get();
    }

    @GetMapping("/feign/notFound")
    public String feignNotFound(){
        return echoService.notFound();
    }

    @GetMapping("/feign/notFound2")
    public String defaultNotFound(Integer id){
        return echoService.notFound(id);
    }
}
