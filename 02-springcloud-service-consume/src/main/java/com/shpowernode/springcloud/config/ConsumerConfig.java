package com.shpowernode.springcloud.config;

import com.alibaba.cloud.nacos.ribbon.NacosRule;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

@Configuration
public class ConsumerConfig {

    @LoadBalanced //负载均衡
    @Bean
    public RestTemplate restTemplate(){
        return new RestTemplate();
    }

    // 在配置文件里配置
    // @Bean
    //       public NacosRule nacosRule(){
    //     return new NacosRule();
    // }
}
