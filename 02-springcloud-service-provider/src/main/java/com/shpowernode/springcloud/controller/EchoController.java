package com.shpowernode.springcloud.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class EchoController {

    @GetMapping("/")
    public String get(){
        return "springcloud nacos is run.....!  provider01";
    }

    @GetMapping("/notFound")
    public String notFound(){
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return "notFound provider01";
    }

    @RequestMapping("/notify")
    public void notify(@RequestBody Object object){
        System.out.println("fallback!--" + object.toString());
    }
}
