package com.shpowernode.springcloud.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class EchoController {

    @GetMapping("/")
    public String get(){
        return "springcloud nacos is run.....! provider02";
    }

    @GetMapping("/notFound")
    public String notFound(){
        return "notFound provider02";
    }
}
